class BasicComponent extends HTMLElement {
    constructor() {
        super();

        // let myTag = document.createElement("h1");
        // this.appendChild(myTag);
    }
}

customElements.define('basic-component', BasicComponent, { extends: "h1" });

let expandingList = document.createElement('ul', { is : 'basic-component' })